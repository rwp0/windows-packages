use v5.38;

use Getopt::Long;

# Run as: perl winget.pl --uninstall (Perl on Windows might omit the arguments if set as default interpreter for .pl files)

Getopt::Long::Parser -> new -> getoptions(
  \my %options ,
  'uninstall|u' ,
  'show|s'
); # Program options

my @packages = (
  'fzf' ,
  'sharkdp.bat' ,
  'jqlang.jq' # Previously: stedolan.jq
);            # Winget packages

if ( defined $options{uninstall} ) {
  uninstall( @packages );
}
elsif ( defined $options{show} ) {
  show( @packages );
}
else {
  install( @packages );
}

sub install ( @packages_to_install ) {
  for my $package ( @packages_to_install ) {
    system <<~ "WINGET";
    winget
      install
        --query $package
        --silent
        --exact
        --no-upgrade

        --disable-interactivity
        --verbose
    WINGET
  }
}

sub uninstall ( @packages_to_uninstall ) {
  for my $package ( @packages_to_uninstall ) {
    system <<~ "WINGET";
    winget
      uninstall
        --query $package
        --silent
        --exact

        --disable-interactivity
        --verbose
    WINGET
  }
}

sub show ( @packages_to_show ) {
  for my $package ( @packages_to_show ) {
    system <<~ "WINGET";
    winget
      show
        --query $package
        --exact

        --disable-interactivity
        --verbose
    WINGET
  }
}

=pod

C<winget> comes from F<C:\Users\EAslanov\AppData\Local\Microsoft\WindowsApps\winget.exe>

Run C<winget install --help> for option descriptions.

Read more at L< https://aka.ms/winget-command-help>.

# Windows Packages

My collection of Windows packages to install
using `winget` Windows Package Manager

Running the `packages.winget.pl` Perl script installs the packages
from the `packages.yaml` file which lists the package names to be installed.

The script can also uninstall and show detailed information about the packages in bulk.
